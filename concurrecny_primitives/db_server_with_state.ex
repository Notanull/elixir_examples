defmodule DbServerWithState do
  def start do
    spawn(fn ->
      connection = :rand.uniform(1000)
      loop(connection)
    end)
  end

  # start/0 creates the long-running process. This is ensured in the private loop/0 function, which waits for a message,
  # handles it, and finally calls itself, thus ensuring that the process never stops
  defp loop(connection) do
    receive do
      {:run_query, from_pid, query_def} ->
        send(from_pid, {:query_result, run_query(connection, query_def)})
      {:query_result,result} ->
        IO.puts(result)
    end

    loop(connection+1)
  end
  # The function start/0 is called by clients and runs in a client process. The private function loop/0 runs in the server process.

  defp run_query(connection, query_def) do
    Process.sleep(3000)
    "Connection #{connection}: #{query_def} result"
  end

  def run_async(server_pid, query_def) do
    send(server_pid,  {:run_query, self(), "#{query_def} has pid #{inspect server_pid}"}) # To Check Which Process executed it from the pool
  end

  def get_result do
    receive do
      {:query_result, result} -> result
    after
      5000 -> {:error, :timeout}
    end
  end
end

# c("db_server_with_state.ex")
#
# pool = Enum.map(1..100, fn _ -> DbServerWithState.start() end)
#
# Enum.each(1..20,
#   fn query_def ->
#     server_pid = Enum.at(pool, :rand.uniform(100) - 1)
#     DbServerWithState.run_async(server_pid, query_def)
#   end)
# Enum.each(1..20,
#   fn query_def ->
#     server_pid = Enum.at(pool, :rand.uniform(100) - 1)
#     DbServerWithState.run_async(server_pid, query_def)
#     DbServerWithState.run_async(server_pid, query_def)
#   end)
#
# Enum.map(1..20, fn _ -> DbServerWithState.get_result() end)
