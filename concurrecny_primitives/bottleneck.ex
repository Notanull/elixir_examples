defmodule Bottleneck do
  def start do
    spawn(fn -> loop() end)
  end


  def send_msg(server, message) do
    IO.puts "============send_msg Process PID=============="
    IO.inspect self()
    IO.puts "============send_msg Process PID=============="
    send(server, {self(), message})
    receive do
      {:response, response} -> response
    end
  end

  defp loop() do
    IO.puts "===========Loop Process PID==============="
    IO.inspect self()
    IO.puts "============Loop Process PID=============="
    receive do
      {caller, msg} ->
        Process.sleep(1000)
        send(caller, {:response, msg})
    end
    loop()
  end

end


# c("bottleneck.ex")
# server_pid = Bottleneck.start()
# Enum.each(1..20,fn n ->  Bottleneck.send_msg(server_pid,"#{n}") end);
#
# Enum.each( 1..5,
#           fn n ->
#             spawn(fn ->
#               IO.puts("Sending msg ##{n}")
#               response = Bottleneck.send_msg(server_pid, n)
#               IO.puts("Response: #{response}")
#             end)
#           end )
