defmodule PooledDbServer do
  # start/0 is the so-called interface function that is used by clients to start the server process.
  def start do
    spawn(&loop/0)
  end

  # start/0 creates the long-running process. This is ensured in the private loop/0 function, which waits for a message,
  # handles it, and finally calls itself, thus ensuring that the process never stops
  defp loop() do
    receive do
      {:run_query, caller, query_def} ->
        send(caller, {:query_result, run_query(query_def)})
      {:query_result,result} ->
        IO.puts(result)
    end

    loop()
  end
  # The function start/0 is called by clients and runs in a client process. The private function loop/0 runs in the server process.

  defp run_query(query_def) do
    Process.sleep(3000)
    "#{query_def} result"
  end

  def run_async(server_pid, query_def) do
    send(server_pid,  {:run_query, self(), "#{query_def} has pid #{inspect server_pid}"}) # To Check Which Process executed it from the pool
  end

  def get_result do
    receive do
      {:query_result, result} -> result
    after
      5000 -> {:error, :timeout}
    end
  end
end

# c("pooled_db_server.ex")
#
# pool = Enum.map(1..100, fn _ -> PooledDbServer.start() end)
#
# Enum.each(1..20,
#   fn query_def ->
#     server_pid = Enum.at(pool, :rand.uniform(100) - 1)
#     PooledDbServer.run_async(server_pid, query_def)
#   end)
#
# Enum.map(1..20, fn _ -> PooledDbServer.get_result() end)
