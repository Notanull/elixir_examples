defmodule DatabaseServer do

  # start/0 is the so-called interface function that is used by clients to start the server process.
  def start do
    spawn(&loop/0)
  end

  # start/0 creates the long-running process. This is ensured in the private loop/0 function, which waits for a message,
  # handles it, and finally calls itself, thus ensuring that the process never stops
  defp loop() do
    receive do
      {:run_query, caller, query_def} ->
        send(caller, {:query_result, run_query(query_def)})
      {:query_result,result} ->
        IO.puts(result)
    end

    loop()
  end
  # The function start/0 is called by clients and runs in a client process. The private function loop/0 runs in the server process.

  defp run_query(query_def) do
    Process.sleep(3000)
    "#{query_def} result"
  end

  def run_async(server_pid, query_def) do
    send(server_pid,  {:run_query, self(), "#{query_def} has pid #{inspect server_pid}"})
  end

  def get_result do
    receive do
      {:query_result, result} -> result
    after
      5000 -> {:error, :timeout}
    end
  end

end

# c("database_server.ex")
# server_pid = DatabaseServer.start()
# DatabaseServer.run_async(server_pid, "query 1")
# DatabaseServer.get_result()
# DatabaseServer.run_async(server_pid, "query 2")
# DatabaseServer.run_async(server_pid, "query 3")
# DatabaseServer.get_result()
# DatabaseServer.get_result()
# DatabaseServer.get_result()  //Here it will wait for timeout period will return {:error, :timeout} since there is no msg in the Queue

# results = Enum.map(1..20, fn n -> DatabaseServer.run_async(server_pid, "query #{n}") end) // Here since every function call is
# //taking atleast three seconds the whole query will take 60 seconds to return the result.

# //i.e if we hit the get_result() the response will be printed only after 60 sec of starting the process and sending the msgs.
# This is because we are using send(dest,msg) not spawning a new process as in previous examples. So in here basically we are
# sending message to a single process only which is thread safe and will process one msg at a time
# results = Enum.map(1..20, fn _ -> DatabaseServer.get_result() end)
