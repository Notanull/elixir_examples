defmodule CollectingQueryResult do


  defp run_query(query_def) do
    Process.sleep(3000)
    "#{query_def} result"
  end

  defp async_query_print_result(query_def) do
    spawn(fn -> IO.puts(run_query(query_def)) end)
  end

  # Instead of printing to the screen, let’s make the lambda send the query result to the caller process:
  # Keep in mind that the result of self/0 depends on the calling process. If you didn’t store the result to the caller variable,
  # and you tried to send(self(), ...) from the inner lambda, it would have no effect.
  # The spawned process would send the message to itself, because calling self/0 returns the pid of the process
  # which invoked the function.
  # This runs all the queries concurrently, and the result is stored in the mailbox of the caller process.
  # In this case, this is the shell (iex) process.
  # Notice that the caller process is neither blocked nor interrupted while receiving messages.
  # Sending a message doesn’t disturb the receiving process in any way.
  defp async_query_return_result(query_def) do
    caller = self()
    spawn(fn ->
      send(caller, {:query_result, run_query(query_def)}) end
      )
    end

  # The only thing affected is the content of the receiving process’s mailbox. Messages remain in the mailbox until they’re consumed or the process terminates.
  # To get the results. First you make a lambda that pulls one message from the mailbox and extracts the query result from it:
  defp get_result() do
      receive do
        {:query_result, result} -> result
      end
  end

  # calling self/0 returns PID of process which invoked the function.
  def runEnum() do
    1..5
    |> Enum.map(&async_query_return_result("query #{&1}"))
    |> Enum.map(fn _ -> get_result() end)  # To pull all the messages from the mailbox into a single list:
    # Enum.each(1..5, &async_query_return_result("query #{&1}"))
  end

  # running below function whihc run 5 queries, it will take 10 seconds to get all the results: because
  # Here, the code sleeps for 2 seconds to simulate a long-running operation. When you call
  # the run_query lambda, the shell is blocked until the lambda is done:
  def runEnum1() do
    Enum.map(1..5, &run_query("query #{&1}"))
  end

  def runEnum2() do
    Enum.each(1..5, &async_query_print_result("query #{&1}"))
  end
end

# c("collecting_query_result.ex")
# CollectingQueryResult.runEnum1()            this Function will block ther terminal untill the time is elapsed(3*5=15s later) and after time elapsed(3*5=15s later Since its not running in parallel i.e only one process is running) result will be printed
# CollectingQueryResult.runEnum2()             this function will return immediately and other task can be executed in same termial. After 3sec result will be printed. Since all task are running in seprate process
# CollectingQueryResult.runEnum()             using Messaeg passing
