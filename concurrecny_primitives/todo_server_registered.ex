defmodule TodoServer do

  alias Todolist

  def start() do
    spawn(fn ->
      Process.register(self(), :todo_server)
      loop(Todolist.new())
    end)
  end

  def loop(state) do
    new_todo_list =
      receive do
        message -> process_message(state, message)
      end
    loop(new_todo_list)
  end

  def add_entry(new_entry) do
    send(:todo_server, {:add_entry, new_entry})
  end

  def entries(date) do
    send(:todo_server, {:entries, self(), date})
    receive do
      {:todo_entries, entries} -> entries
    after
      300 -> {:error, :timeout}
    end
  end

  defp process_message(state, {:add_entry, new_entry}) do
    Todolist.add_entry(state, new_entry)
  end

  defp process_message(state, {:entries, caller, date}) do
    send(caller, {:todo_entries, Todolist.enteries(state, date)})
    state
  end

  defp process_message(state, _), do: state
end


defmodule Todolist do
  defstruct auto_id: 1, entries: %{}

  # def new(), do: %TODOList{}

  def new(entries \\ [] ) do
    # Enum.reduce(enumerable, acc, fun) (enumerable[], acc) -> add_entry(acc, enumberable)
    # Enum.reduce([1, 2, 3], 0, fn(x, acc) -> x + acc end)
    Enum.reduce(entries, %Todolist{}, &add_entry(&2, &1))
  end

  def add_entry(todo_list, entry) do
     entry = Map.put(entry, :id, todo_list.auto_id)
     new_entries = Map.put(todo_list.entries, todo_list.auto_id, entry)
     %Todolist{ todo_list |
       entries: new_entries,
       auto_id: todo_list.auto_id + 1
     }
  end

  def enteries(todo_list, date) do
    todo_list.entries
    |> Stream.filter(fn {_, entry} -> entry.date == date end)
    |> Enum.map(fn {_, entry} -> entry end)
  end

  def update_entry(todo_list, entry_id, updater_fn) do
    case Map.fetch(todo_list.entries, entry_id) do
      :error ->
        todo_list
      {:ok, old_entry}  ->
         old_entry_id = old_entry.id
         new_entry= %{id: ^old_entry_id} = updater_fn.(old_entry)
         new_entries = Map.put(todo_list.entries, old_entry_id , new_entry)
         %Todolist{todo_list | entries: new_entries}
    end
  end

  def update_entry(todo_list, %{} = new_entry) do
    update_entry(todo_list, new_entry.id, fn _ -> new_entry end)
  end

  def delete_entry(todo_list, entry_id) do
    %Todolist{todo_list | entries: Map.delete(todo_list.entries, entry_id)}
  end
end


# This Server can maintain lakh and millions of todoList diffrent process ...  try this..
# iex
# :observer.start()   // check the number of process running should be equla to defualt number of process starteed by iex say x
# Chekc the limit of process that can be created shown in tab opened with above command.. try to create more process than the limit also.
# c("todo_server.ex")
# // check the number of process running (in the observer screen opened earlier) should by x+1
# pool = Enum.map(1..100_000, fn _ -> TodoServer.start() end)
# // check the number of process running (in the observer screen opened earlier) should by x+1+100_000
#   pool1 = Enum.map(1..100_00,
#   fn query_def ->
#     server_pid = Enum.at(pool, :rand.uniform(100_000) - 1)
#     TodoServer.add_entry(server_pid, %{date: ~D[2018-12-20], title: "Shopping-#{query_def}"})
#   end)
# #
# entries = Enum.map(Enum.uniq(pool1),
#   fn server_pid ->
#     %{pid: server_pid , entries: TodoServer.entries(server_pid, ~D[2018-12-20])}
#   end)
