#registered_key_value_store
defmodule RegisteredKeyValueStore do
  use GenServer

  @impl GenServer
  def init(_) do
    {:ok, %{}}
  end

  @impl GenServer
  def handle_cast({:put, key, value}, state) do
    {:noreply, Map.put(state, key, value)}
  end

  @impl GenServer
  def handle_call({:get, key}, _, state) do
    {:reply, Map.get(state,key), state}
  end

  def handle_info(:cleanup, state) do
    IO.puts "performing cleanup..."
    {:noreply, state}
  end

  @impl GenServer
  def handle_info( unknown_message, state) do
    super(unknown_message, state)
  end

  def start() do
    GenServer.start(RegisteredKeyValueStore, nil, name: RegisteredKeyValueStore)
  end

  def put(key, value) do
    GenServer.cast(RegisteredKeyValueStore, {:put, key, value})
  end

  def get(key) do
    GenServer.call(RegisteredKeyValueStore, {:get, key})
  end

end
