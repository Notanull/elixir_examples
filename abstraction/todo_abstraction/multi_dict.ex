defmodule TodoAbstraction.MultiDict do
  def new(), do: %{}

  def add(dict, key, value) do
    #&([title | &1]) => fn titles -> [title | titles] end   Annonymous  Fucntion
    # update(map,key, initial, fn) if key already present in map then function will be applied on the map(key) value as argument to function
    # If Key not present in map then fn is ignored and initial value is setted as map(key)
    # If Key is not present in map => Eq. map(key) = initial
    # if Key is present in map => Eq.  map(key) = fn(map(key))
    Map.update(dict, key, [value], &[value | &1])
  end

  def get(dict, key) do
    Map.get(dict, key, [])
  end
end
