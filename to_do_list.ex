 defmodule ToDoList do
  def new(), do: %{}  # Map

  def addItem(todo_list, date, title) do
    #&([title | &1]) => fn titles -> [title | titles] end   Annonymous  Fucntion
    # update(map,key, initial, fn) if key already present in map then function will be applied on the map(key) value as argument to function
    # If Key not present in map then fn is ignored and initial value is setted as map(key)
    # If Key is not present in map => Eq. map(key) = initial
    # if Key is present in map => Eq.  map(key) = fn(map(key))
    Map.update(todo_list, date, [title], &([title | &1]))
  end

  def entries(todo_list, date) do
    Map.get(todo_list, date, [])
  end
end


# todo_list =
#           TodoList.new() |>
#             TodoList.add_entry(~D[2018-12-19], "Dentist") |>
#             TodoList.add_entry(~D[2018-12-20], "Shopping") |>
#             TodoList.add_entry(~D[2018-12-19], "Movies")
# iex(2)> TodoList.entries(todo_list, ~D[2018-12-19])
# ["Movies", "Dentist"]
# iex(3)> TodoList.entries(todo_list, ~D[2018-12-18])
# []
